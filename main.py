import requests
import datetime
import logging
from bson import json_util
import json

token = "D66295F813A673E1076D0E0B1F7148DF"

def pega_steamid(nick):
    r = requests.get(f'http://steamcommunity.com/id/{nick}')
    r = r.text
    index = r.find('steamid')
    id = r[index - 1: index + 100].split(':')[1].split('"')[1]
    return id

def pega_dados_game(steam_id):
    url = f'http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=730&key={token}&steamid={steam_id}'
    r = requests.get(url)

    if r.status_code == 200:
        r = r.text
        r = json.loads(r)
        return r

    return False

def pega_dados_usuario(steam_id):
    url = f'http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key={token}&steamids={steam_id}'
    r = requests.get(url)
    r = r.text
    r = json.loads(r)
    return r

def gera_dados(steam_id):

    try:
        dados_usuario = pega_dados_usuario(steam_id)
        dados_usuario = {'data_user': dados_usuario['response']['players'][0]}
        dados_game = pega_dados_game(steam_id)

        if dados_game:

            dados_game = dados_game['playerstats']['stats']

            data_game = {'data_game' : {}}
            data_game.update({'date': datetime.datetime.now()})

            for dado in dados_game:
                d = {f'{str(dado["name"]).replace(".", "_")}': dado["value"]}
                data_game['data_game'].update(d)

            dados_usuario.update(data_game)

            total_kills = {'total_kills' : dados_usuario['data_game']['total_kills']}
            total_deaths = {'total_deaths' : dados_usuario['data_game']['total_deaths']}
            id = {'steamid': dados_usuario['data_user'].get('steamid')}
            id.update(total_kills)
            id.update(total_deaths)
            id = {'_id': id}
            id.update(dados_usuario)

            return id

    except Exception as e:

        logging.ERROR(f'Erro ao gerar dados {e}')

        return None


def manda_para_deagle(dados):
    requests.post("https://oxo8zr3qy3.execute-api.us-east-1.amazonaws.com/dev/csgo/deagle/user", data=dados)


def pega_lista_usuarios_deagle():
    response = requests.post("https://oxo8zr3qy3.execute-api.us-east-1.amazonaws.com/dev/csgo/deagle/user", data=json.dumps({"for_deagle":"get_users"}, default=json_util.default))
    dados = response.text
    dados = eval(dados)
    dados = dados["body"]['usuarios']
    return dados


def main(event,context):
    lista_usuarios = pega_lista_usuarios_deagle()
    for user in lista_usuarios:
        dados = gera_dados(user['_id'])
        if dados is not None:
            dados.update({'for_deagle':'insert'})
            dados = json.dumps(dados, default=json_util.default)
            manda_para_deagle(dados)
