# CSGO_ETL
Captura dados de alguns jogadores e salva em base de dados nao estruturados

## Dependencias
As dependendcias do projeto estão no requirements.txt

#### Instalando as dependencias:
`pip install -r requirements.txt`

## Subindo projeto no AWS Lambda
Para "subir" o projeto no AWS Lambda precisamos de todas as libs dentro da pasta raiz do projeto.

#### instalando as libs na pasta raiz

`pip install -t . -r requirements.txt`